/*
 * main.cpp
   Copyright (C) 2023 oli <oli@s3rv3r>
 *
 * Distributed under terms of the MIT license.
 */

#include <stdio.h>
#include "position.h"

int main(int argc, char **argv) {
  printf("Hello world!\n");

  // Examples for positions and trajectories
  struct Position p1 = { .t = 1.0, .x = 0.0, .v = -2.0};
  printPosition(&p1);
  printf("\n");

  struct Trajectory traj = makeTrajectory();
  for (double x=0; x < 5.0; x++) {
    struct Position p = { .t = x, .x = x, .v = 1.0 };
    appendPosition(&traj, p);
  }
  printTrajectory(&traj);

  return 0;
}


