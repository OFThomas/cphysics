/*
 * forces.c
 * Copyright (C) 2023 oli <oli@s3rv3r>
 *
 * Distributed under terms of the MIT license.
 */

#include "forces.h"

struct Force gravity(struct Position position) {
  struct Force force;
  force.x = EARTH_GRAVITY;
  return force;
}
