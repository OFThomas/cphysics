/*
 * forces.h
 * Copyright (C) 2023 oli <oli@s3rv3r>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef FORCES_H
#define FORCES_H

#include "position.h"

// the first force is gravity 
// we'll take a position and return the updated position
#define EARTH_GRAVITY -9.81

// for each particle you go through all the forces acting on it
// and sum them up to get the net force
// then use use f / m = a to calculate the acceleration 
// then use the acceleration to update the velocity
// then use the velocity to update the position
struct Force {
  double x;
};

struct Force gravity(struct Position position);

// f = m * a
// x += (v * t) + (0.5 * a * t * t)
// v += a * t

#endif /* !FORCES_H */
