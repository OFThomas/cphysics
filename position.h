/**
 * position.h
 *
 *
 */

#ifndef POSITION_H
#define POSITION_H

#include <stdlib.h>
#include <stdio.h>

struct Position {
    double t;
    double x;
    double v;
};

/// Array of positions defines location of particle over time
/// Use malloc to dynamically resize
struct Trajectory {
    struct Position *array;
    size_t num_positions;
    size_t alloc_length;
};

void printPosition(struct Position *pos);
struct Position *nthPosition(struct Trajectory *traj, size_t n);
void printTrajectory(struct Trajectory *traj);
struct Trajectory makeTrajectory();

/// Add a new position to the end of the trajectory array
void appendPosition(struct Trajectory *traj, struct Position pos);

#endif
