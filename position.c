#include "position.h"

void printPosition(struct Position *pos) {
  printf("(t=%f,x=%f,v=%f)", pos->t, pos->x, pos->v);
}

/// You can only use this function to read from the trajectory, not
/// write to it.
struct Position *nthPosition(struct Trajectory *traj, size_t n) {
  if (n >= traj->num_positions) {
    printf("Trajectory only has %ld positions; n=%ld is invalid", traj->num_positions, n);
    exit(-1);
  }
  return traj->array + n;
}

void printTrajectory(struct Trajectory *traj) {
  printf("[");
  for (int n = 0; n < traj->num_positions; n++) {
    printPosition(nthPosition(traj, n));
    if (n < traj->num_positions-1)
      printf(",\n");
  }
  printf("]");
}

struct Trajectory makeTrajectory() {
  // TODO make this a properly resizing array (fixed at 128 positions for now)
  struct Trajectory traj;
  traj.num_positions = 0;
  traj.alloc_length = 128;
  traj.array = (struct Position *)malloc(traj.alloc_length * sizeof(struct Position));
  return traj;
}

/// Add a new position to the end of the trajectory array
void appendPosition(struct Trajectory *traj, struct Position pos) {
  // Extend the trajectory array if needed and update alloc_length
  if (traj->num_positions+1 == traj->alloc_length) {
    printf("Run out of space! Go and implement Trajectory properly");
    exit(-1);
  }
  // Copy pos to the end of the trajectory array and increment num_positions
  *(traj->array + traj->num_positions) = pos;
  traj->num_positions++;
}
